<?php
/**
 * @file
 * Rules inc file for rules module hook.
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_pet_rules_action_info() {
  $actions = array(
    'commerce_pet_action_send_pet' => array(
      'label' => t('Send PET Commerce mail'),
      'group' => t('Previewable email templates'),
      'parameter' => array(
        'pet_name' => array(
          'type' => 'text',
          'label' => t('The previewable email template to use for commerce'),
          'options list' => 'commerce_pet_list',
          'description' => t('The template that will be sent for this action. You can see the full list or add a new one from <a href="@url">this page</a>.', array('@url' => url('admin/structure/pets'))),
        ),
        'to_text' => array(
          'type' => 'text',
          'label' => t('Recipient(s) (for sending to a fixed set of email address(es))'),
          'description' => t('The e-mail address or addresses where the message will be sent to. The formatting of this string must comply with RFC 2822. Either this or the variable recipient below must be provided.'),
          'optional' => TRUE,
          'default value' => NULL,
        ),
        'to_account' => array(
          'type' => 'user',
          'label' => t('Recipient (for sending to a user provided by a Rules event)'),
          'description' => t('Send mail to address on this account. Either this or the fixed recipient(s) above must be provided. If both are provided, this overrides the fixed list.'),
          'optional' => TRUE,
          'default value' => NULL,
        ),
        'account_subs' => array(
          'type' => 'user',
          'label' => t('User for token substitutions (if any)'),
          'description' => t('If your template includes user tokens, this user will be used for them.'),
          'optional' => TRUE,
          'default value' => NULL,
        ),
        'entity_subs' => array(
          'type' => 'entity',
          'label' => t('Entity for token substitutions (if any)'),
          'description' => t('If your template includes entity tokens, this entity will be used for them.'),
          'optional' => TRUE,
          'default value' => NULL,
        ),
      ),
    ),
    'commerce_pet_action_send_pet_roles' => array(
      'label' => t('Send PET Commerce mail to all users of a role'),
      'group' => t('Previewable email templates'),
      'parameter' => array(
        'pet_name' => array(
          'type' => 'text',
          'label' => t('The previewable email template to use for commerce'),
          'options list' => 'commerce_pet_list',
          'description' => t('The template that will be sent for this action. You can see the full list or add a new one from <a href="@url">this page</a>.', array('@url' => url('admin/structure/pets'))),
        ),
        'roles' => array(
          'type' => 'list<integer>',
          'label' => t('Roles'),
          'options list' => 'entity_metadata_user_roles',
          'description' => t('Select the roles whose users should receive the mail.'),
        ),
        'entity_subs' => array(
          'type' => 'entity',
          'label' => t('Entity for token substitutions (if any)'),
          'description' => t('If your template includes entity tokens, this entity will be used for them.'),
          'optional' => TRUE,
          'default value' => NULL,
        ),
      ),
    ),
  );
  return $actions;
}

/**
 * Callback for eponymous rules action.
 *
 * Rule action - Send PET Commerce mail.
 */
function commerce_pet_action_send_pet($pet_name, $to_text, $to_account, $account_subs, $entity_subs, $settings) {
  $pet = pet_load($pet_name);

  // Resolve the recipient.
  if (isset($to_account)) {
    $pet_to = $to_account->mail;
  }
  elseif (isset($to_text)) {
    $pet_to = $to_text;
  }
  else {
    watchdog('pet', 'Mail send using %name PET failed. No recipient provided.', array('%name' => $pet_name), WATCHDOG_ERROR);
    return;
  }

  $params = array(
    'pet_from' => variable_get('site_mail', ini_get('sendmail_from')),
    'pet_to' => $pet_to,
    'pet_uid' => isset($account_subs) ? $account_subs->uid : NULL,
    'pet_cc' => pet_parse_mails(pet_isset_or($pet->cc_default)),
    'pet_bcc' => pet_parse_mails(pet_isset_or($pet->bcc_default)),
  );

  if (isset($entity_subs)) {
    $type = $entity_subs->getBundle();
    if ($type == "commerce_order") {
      $params['pet_order_id'] = $entity_subs->getIdentifier();
    }
    elseif ($type == "commerce_return") {
      $params['pet_return_id'] = $entity_subs->getIdentifier();
    }
  }
  pet_send_one_mail($pet, $params);
}


/**
 * Callback for eponymous rules action.
 *
 * Rule action - Send PET Commerce mail to all users of a role.
 */
function commerce_pet_action_send_pet_roles($pet_name, $rids, $entity_subs, $settings) {
  $pet = pet_load($pet_name);

  if (in_array(DRUPAL_AUTHENTICATED_RID, $rids)) {
    $result = db_query('SELECT mail, uid FROM {users} WHERE uid > 0');
  }
  else {
    // Avoid sending emails to members of two or more target role groups.
    $query = db_select('users', 'u');
    $query->fields('u', array('mail', 'uid'))
      ->innerJoin('users_roles', 'r', 'u.uid = r.uid');
    $query->condition('r.rid', $rids, 'IN');
    $query->groupBy('u.uid');
    $result = $query->execute();
  }

  $params = array(
    'pet_from' => variable_get('site_mail', ini_get('sendmail_from')),
    'pet_cc' => pet_parse_mails(pet_isset_or($pet->cc_default)),
    'pet_bcc' => pet_parse_mails(pet_isset_or($pet->bcc_default)),
  );

  if (isset($entity_subs)) {
    $type = $entity_subs->getBundle();
    if ($type == "commerce_order") {
      $params['pet_order_id'] = $entity_subs->getIdentifier();
    }
    elseif ($type == "commerce_return") {
      $params['pet_return_id'] = $entity_subs->getIdentifier();
    }
  }
  foreach ($result as $row) {
    $params['pet_to'] = $row->mail;
    $params['pet_uid'] = $row->uid;
    pet_send_one_mail($pet, $params);
  }
}

/**
 * Return list of all PETs for rules configuration.
 */
function commerce_pet_list() {
  $list = array();
  foreach (pet_load_multiple(FALSE) as $pet) {
    $list[$pet->name] = $pet->title;
  }
  return $list;
}
