Commerece Previewable Email Templates (commerce_pet)
====================================================
This module allows commerce notification via Previewable Email Templates (pet)
module. This module has rule actions to trigger commerce notification mail via
PET, so email actions can be triggered by any Rule.

The Previewable Email Template (PET) module lets users create email templates,
with token substitution, which can be previewed before sending. The emails can
be sent to one or many email addresses in a flexible way, and the recipients
may or may not be Drupal account holders (users).

Tokens are available for commerce_order entity and it's sub entity like line
items, commerce total, address profile.

This module supports the Rules module, so email actions can be triggered by any
Rule.

This module supports the Commerce RMA (commerce_rma) module, tokens are
available for commerce_return entity.

Required Modules
================

- Previewable Email Templates (PET)
- Drupal Commerce (commerce)

Optional Modules
================

- Rules
- Tokens
- Commerce RMA (commerce_rma)

Installation
============
* Copy the commerce_pet directory to the modules folder in your installation.
* Go to the Modules page (/admin/modules) and enable it.

Please see PET modole for PETs features and documents.
